﻿using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;

namespace SimpleSlavery
{
	public class Hediff_EmancipateFix : HediffWithComps
	{
		public Faction actualFaction = null;
		public override void PostTick ()
		{
			base.PostTick ();
			if (this.ageTicks > 1) {
				pawn.SetFaction (actualFaction);
				pawn.guest.SetGuestStatus (Faction.OfPlayer, true);
				if (pawn.GetRoom ().isPrisonCell) {
					pawn.guest.interactionMode = PrisonerInteractionModeDefOf.NoInteraction;
				}
				else{
					pawn.guest.interactionMode = PrisonerInteractionModeDefOf.Release;
					pawn.guest.released = true;
				}
				pawn.health.RemoveHediff (this);
			}
		}
	}

	public class Hediff_SlaveMemory : HediffWithComps
	{
		// Saved work priority settings
		public Dictionary<WorkTypeDef, int> savedWorkPriorities = new Dictionary<WorkTypeDef, int>{};
		// Saved willpower
		public float savedWillpower = 0;

		public override void PostMake ()
		{
			base.PostMake ();
			SaveMemory ();
		}

		public void SaveMemory()
		{
			foreach(WorkTypeDef work in DefDatabase<WorkTypeDef>.AllDefs){
				if (!savedWorkPriorities.ContainsKey (work)){
					savedWorkPriorities.Add(work, pawn.workSettings.GetPriority(work));
				}
				else{
					savedWorkPriorities[work] = pawn.workSettings.GetPriority (work);
				}
			}
			savedWillpower = ((Hediff_Enslaved)pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Enslaved"))).SlaveWillpower;
		}

		public override void ExposeData ()
		{
			base.ExposeData ();
			Scribe_Values.Look<float> (ref savedWillpower, "savedWillpower");
			Scribe_Collections.Look<WorkTypeDef, int> (ref savedWorkPriorities, "savedWorkPriorities");
		}
	}


	public class Hediff_Enslaved : HediffWithComps
	{
		const int minHoursBetweenEscapeAttempts = 24;
		const int maxWillpower = 100;

		public Faction actualFaction = null;
		public Faction slaverFaction = null;
		float willpower = 100;
		int hoursSinceLastEscapeAttempt = 6;
		public bool waitingInJail = false;
		public bool isMovingToEscape = false;

		public void SaveMemory()
		{
			Hediff_SlaveMemory slaveMemory = null;
			if (!pawn.health.hediffSet.HasHediff (HediffDef.Named ("SlaveMemory"))) {
				pawn.health.AddHediff (HediffDef.Named ("SlaveMemory"));
			}
			slaveMemory = pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("SlaveMemory")) as Hediff_SlaveMemory;
			slaveMemory.SaveMemory ();
		}
		public void LoadMemory()
		{
			if (pawn.health.hediffSet.HasHediff(HediffDef.Named("SlaveMemory"))) {
				Hediff_SlaveMemory memory = pawn.health.hediffSet.GetFirstHediffOfDef(HediffDef.Named("SlaveMemory")) as Hediff_SlaveMemory;
				foreach (KeyValuePair<WorkTypeDef, int> workPriority in memory.savedWorkPriorities) {
					pawn.workSettings.SetPriority (workPriority.Key, workPriority.Value);
				}
				willpower = memory.savedWillpower;
			}
			else
				Log.Error ("[SimpleSlavery]: Failed to find SlaveMemory hediff for pawn " + pawn.NameStringShort+".");
		}

		public float SlaveWillpower {
			get {
				return willpower;
			}
		}

		public override void PostMake ()
		{

			base.PostMake ();
			actualFaction = pawn.Faction;
			slaverFaction = (Faction.OfPlayer);
			pawn.SetFaction (Faction.OfPlayer);

			// If the hediff was added without equipping a slave collar, ensure they get one
			if(pawn.apparel.WornApparel.Find(SlaveUtility.IsSlaveCollar) == null)
				SlaveUtility.GiveSlaveCollar (pawn);

			// Certain backstories begin with no willpower
			if (pawn.story.childhood.Title == "Vatgrown slavegirl")
				willpower = 0;
			if( pawn.story.adulthood != null) {
				if (pawn.story.adulthood.Title == "Urbworld sex slave")
					willpower = 0;
			}

			// We were freed, but then enslaved AGAIN
			if (pawn.health.hediffSet.HasHediff (HediffDef.Named ("SlaveMemory"))) {
				SlaveAgain ();
				// Take a willpower hit, but only if we were free for a while
				if (pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("SlaveMemory")).ageTicks > 10000) {
					TakeWillpowerHit (50);
				}
			}
		}

		public override void Tick ()
		{
			base.Tick ();

			// Each day
			// the pawn loses some willpower
			if (pawn.IsHashIntervalTick (60000)) {
				// Make sure we're not already at rock bottom
				if (willpower > 0) {

					TakeWillpowerHit (10);
				}
			}

			// Break here if we're not spawned on a map
			if (!pawn.Spawned)
				return;

			// Every second or so
			if (pawn.IsHashIntervalTick (60 * 1)) {
				// Return to slave-state
				if (waitingInJail && hoursSinceLastEscapeAttempt >= 6 && !pawn.Downed && !pawn.jobs.curDriver.asleep) {
					Log.Message ("DEBUG: " + pawn.NameStringShort + " has returned to being a slave.");
					SlaveAgain ();
				}
				// Looks like we got interrupted while moving to escape
				if (isMovingToEscape && pawn.CurJob.def.defName != "Goto")
					isMovingToEscape = false;
				// Run!
				if (isMovingToEscape && pawn.GetRoom().TouchesMapEdge) {
					TryToEscape ();
				}
			}

			// Every three hours
			if (pawn.IsHashIntervalTick (2500 * 3)) {
				if(hoursSinceLastEscapeAttempt < 72)
					hoursSinceLastEscapeAttempt += 1;
				// The pawn will consider escape
				if(willpower > 0 &&
					pawn.health.capacities.CanBeAwake &&
					pawn.health.capacities.CapableOf(PawnCapacityDefOf.Consciousness) &&
					pawn.health.capacities.CapableOf(PawnCapacityDefOf.Moving) &&
					!pawn.health.Downed &&
					!pawn.jobs.curDriver.asleep &&
					pawn.Faction == Faction.OfPlayer
					)
				ConsiderEscape ();
			}
		}

		public void TakeWillpowerHit(float severity)
		{
			if (pawn.story.traits.HasTrait (TraitDef.Named ("Wimp")))
				severity *= 2;
			if (pawn.story.traits.HasTrait (TraitDef.Named ("Nerves"))) { 
				int nerveDegree = pawn.story.traits.GetTrait (TraitDef.Named ("Nerves")).Degree;
				if (nerveDegree > 0)
					severity /= nerveDegree;
				else if (nerveDegree < 0)
					severity *= nerveDegree;
			}

			willpower -= (float)Math.Abs(severity * 0.1);
			if (willpower < 0)
				willpower = 0;
			Log.Message ("DEBUG: Slave " + pawn.NameStringShort + " Willpower = " + willpower);
		}

		public void SetWillpowerDirect(int newWill){
			willpower = Math.Min(Math.Max(newWill,0), maxWillpower);
		}

		// Try to drum up the courage to escape
		public void ConsiderEscape()
		{
			// 10% chance to consider escaping
			if (!pawn.guest.IsPrisoner && Rand.Chance (0.1f) && !pawn.GetRoom ().isPrisonCell && !isMovingToEscape){
				//if (true) {
				if (hoursSinceLastEscapeAttempt > minHoursBetweenEscapeAttempts) {
					// If we're not in a "room" aka free to run outside, then chance improves
					int combined_bonus = 0;
					if (pawn.GetRoom ().TouchesMapEdge)
						combined_bonus += 10;
					if (pawn.GetRoom ().PsychologicallyOutdoors)
						combined_bonus += 7;
					if (pawn.story.traits.HasTrait (TraitDef.Named ("Nerves")))
						combined_bonus += pawn.story.traits.GetTrait (TraitDef.Named ("Nerves")).Degree * 10;
					// Health malus
					combined_bonus -= (int)((1 - pawn.health.summaryHealth.SummaryHealthPercent) * 50);
					// Take hours since last attempt into account
					int time_bonus = (int)Math.Round ((Math.Max (hoursSinceLastEscapeAttempt, 72) * (willpower / 100)) * 0.2083f);

					// Do a willpower check
					if (Math.Round (willpower) + combined_bonus + time_bonus > Rand.Range (1, maxWillpower)) {
						if (pawn.GetRoom ().TouchesMapEdge)
							TryToEscape ();
						else
							MoveToEscape ();
					}
				}
			}
		}

		public void MoveToEscape()
		{
			IntVec3 c;
			if(RCellFinder.TryFindBestExitSpot (pawn, out c)){
				pawn.jobs.StartJob (new Job (JobDefOf.Goto, c), JobCondition.InterruptForced);
				isMovingToEscape = true;
			}
		}

		// Freedom!!!
		public void TryToEscape()
		{
			isMovingToEscape = false; // We've moved into place to escape already
			hoursSinceLastEscapeAttempt = 0; // Reset time tracker
			SaveMemory (); // Save our work priorities and willpower to the external hediff
			//Messages.Message ("Slave " + pawn.NameStringShort + " is attempting to escape!", pawn, MessageSound.SeriousAlert);
			pawn.SetFaction (actualFaction); // Revert to real faction
			pawn.guest.SetGuestStatus (slaverFaction, true);
			pawn.guest.released = false; // Ensure the slave is not set to released mode
			pawn.guest.interactionMode = PrisonerInteractionModeDefOf.NoInteraction; // Ensure the interaction mode is not "release"
		}

		public void CaughtSlave() {
			waitingInJail = true;
		}

		private void SlaveAgain()
		{
			pawn.SetFaction (Faction.OfPlayer);
			pawn.guest.SetGuestStatus (null);
			LoadMemory ();
			TakeWillpowerHit (100);
			hoursSinceLastEscapeAttempt = 0;
			waitingInJail = false;
		}

		public void Emancipate()
		{
			//TODO: Maybe join the slaverFaction if they were nice or utterly brainwashed
			//pawn.SetFaction (actualFaction);
			SaveMemory ();
			pawn.health.AddHediff (HediffDef.Named ("Hediff_EmancipateFix"));
			((Hediff_EmancipateFix)pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Hediff_EmancipateFix"))).actualFaction = actualFaction;
			pawn.health.RemoveHediff (this);
			// See if the pawn wants to join on emancipation
			if (!pawn.GetRoom ().isPrisonCell) {
				if (willpower <= 1 || // Broken slaves will typically join
					(willpower <= 25 && pawn.needs.mood.CurLevelPercentage > Rand.Range(0.55f,0.95f)) &&
					pawn.story.traits.allTraits.Find(x => x.def.defName == "Nerves" && x.Degree > 0) == null // Iron-willed/steadfast pawns never join on emancipation
				) {// Join the colony
					pawn.guest.isPrisonerInt = false;
					pawn.SetFaction (slaverFaction);
				}
			}
		}

		public override void ExposeData ()
		{
			base.ExposeData ();
			Scribe_References.Look<Faction> (ref slaverFaction, "slaverFaction");
			Scribe_References.Look<Faction> (ref actualFaction, "actualFaction");
			Scribe_Values.Look<float> (ref willpower, "slaveWillpower", 100);
			Scribe_Values.Look<int> (ref hoursSinceLastEscapeAttempt, "escapeHours", 3);
			Scribe_Values.Look<bool> (ref waitingInJail, "waitingInJail", false);
		}

		// Hidden effect
		public override bool Visible {
			get { return false; }
		}
	}
}

