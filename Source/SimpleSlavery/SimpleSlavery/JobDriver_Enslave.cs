﻿using System;
using RimWorld;
using Verse;
using Verse.AI;
using System.Collections.Generic;
using System.Diagnostics;

namespace SimpleSlavery
{
	public class JobDriver_EnslavePrisoner : JobDriver
	{
		const int enslaveDuration = 300;

		private Pawn Victim {
			get {
				return (Pawn)CurJob.GetTarget (TargetIndex.A).Thing;
			}
		}

		private Apparel SlaveCollar {
			get {
				return (Apparel)CurJob.GetTarget (TargetIndex.B).Thing;
			}
		}

		[DebuggerHidden]
		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOnDespawnedOrNull (TargetIndex.A);
			this.FailOnDestroyedOrNull (TargetIndex.B);
			this.FailOnForbidden (TargetIndex.B);
			yield return Toils_Reserve.Reserve (TargetIndex.A);
			yield return Toils_Reserve.Reserve (TargetIndex.B);
			yield return Toils_Goto.GotoThing (TargetIndex.B, PathEndMode.Touch);
			yield return new Toil {
				initAction = delegate {
					pawn.jobs.curJob.count = 1;
				}
			};
			yield return Toils_Haul.StartCarryThing (TargetIndex.B);
			yield return Toils_Goto.GotoThing (TargetIndex.A, PathEndMode.Touch);
			Toil enslave = new Toil();
			enslave.initAction = delegate {
				PawnUtility.ForceWait (Victim, enslaveDuration, pawn, true);
			};
			enslave.defaultDuration = enslaveDuration;
			enslave.WithProgressBarToilDelay(TargetIndex.A, false, -0.5f);
			yield return enslave;
			yield return new Toil
			{
				initAction = delegate
				{
					Thing slaveCollar = null;
					pawn.carryTracker.TryDropCarriedThing(pawn.PositionHeld, ThingPlaceMode.Direct, out slaveCollar, null);
					if(slaveCollar != null){
						Apparel collar = (Apparel)slaveCollar;
						// Do enslave attempt
						bool success = true;

						if (!Victim.jobs.curDriver.asleep &&
							!Victim.story.traits.HasTrait(TraitDef.Named("Wimp")) &&
							!Victim.InMentalState
						){
							if (Victim.story.traits.HasTrait(TraitDef.Named("Nerves")) &&
								(Victim.story.traits.GetTrait(TraitDef.Named("Nerves")).Degree == -2 && Rand.Value > 0.66f) ||
								Victim.needs.mood.CurInstantLevelPercentage < Rand.Range(0f, 0.33f)
							)
								success = false;
						}
						//if((pawn.skills.GetSkill(SkillDefOf.Melee).Level + pawn.skills.GetSkill(SkillDefOf.Social)) / 2
						if(success){
							Log.Message("Enslaved " + Victim.NameStringShort);
							SlaveUtility.EnslavePawn(Victim, collar);
							if (slaveCollar.Stuff.stuffProps.categories.Contains(StuffCategoryDefOf.Metallic) && !Victim.health.hediffSet.HasHediff(HediffDef.Named("SlaveMemory")))
								(Victim.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Enslaved")) as Hediff_Enslaved).TakeWillpowerHit (150);
							Messages.Message("EnslavedPrisonerSuccess".Translate(new []{pawn.NameStringShort, Victim.NameStringShort}), MessageSound.Benefit);
							AddEndCondition (() => JobCondition.Succeeded);
						}else{
							Victim.mindState.mentalStateHandler.TryStartMentalState(MentalStateDefOf.Berserk, "ReasonFailedEnslave".Translate(new []{pawn.NameStringShort, Victim.NameStringShort}));
							AddEndCondition (() => JobCondition.Incompletable);
						}
					}
					else
						AddEndCondition (() => JobCondition.Incompletable);
				},
				defaultCompleteMode = ToilCompleteMode.Instant
			};
		}
	}
}

