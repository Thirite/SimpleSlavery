﻿using System;
using RimWorld;
using Verse;
using System.Collections.Generic;

namespace SimpleSlavery
{
	public static class SlaveUtility{

		public static void EnslavePawn(Pawn pawn, Apparel collar = null){
			if (pawn == null) {
				Log.Error ("[SimpleSlavery] Error: Tried to enslave null pawn.");
				return;
			}
			if (!pawn.RaceProps.Humanlike) {
				Log.Error ("[SimpleSlavery] Error: Tried to enslave a non-humanlike pawn.");
				return;
			}
			if (!SlaveUtility.IsPawnColonySlave(pawn)) {
				SlaveUtility.GiveSlaveCollar (pawn, collar);
				pawn.health.AddHediff (HediffDef.Named ("Enslaved"));
			}
		}

		public static void EmancipatePawn(Pawn pawn){
			if (IsPawnColonySlave (pawn))
				(pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Enslaved")) as Hediff_Enslaved).Emancipate ();
		}

		public static bool IsPawnColonySlave(Pawn pawn)
		{
			return pawn.health.hediffSet.HasHediff (HediffDef.Named ("Enslaved"));
		}

		public static Hediff_Enslaved GetEnslavedHediff(Pawn pawn){
			return pawn.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named("Enslaved")) as Hediff_Enslaved;
		}

		public static bool IsSlaveCollar(Apparel apparel)
		{
			return apparel.def.defName.Contains ("SlaveCollar");
		}

		public static Apparel MakeRandomSlaveCollar()
		{
			var stuff = new List<ThingDef>{
				ThingDef.Named ("Steel"),
				ThingDef.Named ("Silver"),
				ThingDef.Named ("Gold"),
				ThingDef.Named ("Plasteel"),
				ThingDef.Named ("Uranium")
			};
			int chance = (int)Math.Round (Math.Pow(Rand.Value,Math.PI) * (stuff.Count - 1));
			var slaveCollar = ThingMaker.MakeThing (ThingDef.Named ("Apparel_SlaveCollar"), stuff[chance]) as Apparel;
			return slaveCollar;
		}

		public static void GiveSlaveCollar(Pawn pawn, Apparel collar = null)
		{
			if (pawn == null) {
				Log.Error ("Tried to give a collar to a null pawn.");
				return;
			}
			Apparel newCollar = collar;
			if (newCollar == null) 
				newCollar = MakeRandomSlaveCollar ();
			pawn.apparel.Wear (newCollar, true);
			if(pawn.outfits == null)
				pawn.outfits = new Pawn_OutfitTracker ();
			pawn.outfits.forcedHandler.SetForced (newCollar, true);
		}

	}
}

