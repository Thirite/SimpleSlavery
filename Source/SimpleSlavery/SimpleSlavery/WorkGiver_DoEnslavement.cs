﻿using System;
using RimWorld;
using Verse;
using Verse.AI;


namespace SimpleSlavery
{
	public class WorkGiver_Warden_DoEnslavement : WorkGiver_Warden
	{
		//
		// Properties
		//
		public override PathEndMode PathEndMode {
			get {
				return PathEndMode.OnCell;
			}
		}

		//
		// Methods
		//
		public override Job JobOnThing (Pawn pawn, Thing t, bool forced = false)
		{
			if (!ShouldTakeCareOfPrisoner (pawn, t)) {
				return null;
			}
			Pawn prisoner = (Pawn)t;

			if (prisoner.guest.interactionMode != DefDatabase<PrisonerInteractionModeDef>.GetNamed("PIM_Enslave") || SlaveUtility.IsPawnColonySlave(pawn) || !pawn.CanReserve (prisoner, 1, -1, null, false)) {
				return null;
			}
			Apparel slaveCollar = (Apparel)pawn.Map.listerThings.ThingsMatching (
				ThingRequest.ForGroup(ThingRequestGroup.Apparel)).Find(x => SlaveUtility.IsSlaveCollar ((Apparel)x) && !x.IsForbidden (pawn.Faction));
			return slaveCollar != null ? new Job (DefDatabase<JobDef>.GetNamed ("EnslavePrisoner"), prisoner, slaveCollar) : null;
		}
	}
}

