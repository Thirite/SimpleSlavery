﻿using System;
using RimWorld;
using Verse;
using Harmony;
using HugsLib;
using System.Reflection;
using Verse.AI;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.Reflection.Emit;

namespace SimpleSlavery
{
	public class SlaveryBase : ModBase
	{
		public override string ModIdentifier {
			get {
				return "SimpleSlavery";
			}
		}
	}

	[StaticConstructorOnStartup]
	internal static class HarmonyPatches{

		static HarmonyPatches(){

			HarmonyInstance harmonyInstance = HarmonyInstance.Create ("rimworld.thirite.simpleslavery");
			HarmonyInstance.DEBUG = true;
			harmonyInstance.PatchAll(Assembly.GetExecutingAssembly());
		}
	}

	// Add a suffix onto PostApplyDamage to take beatings into account for slave willpower
	[HarmonyPatch(typeof(Pawn), "PostApplyDamage")]
	public static class Pawn_PostApplyDamage_Patch {

		[HarmonyPostfix]
		public static void Beaten(Pawn __instance, ref DamageInfo dinfo){
			// Check if the pawn is enslaved
			if(SlaveUtility.IsPawnColonySlave(__instance)){
				Hediff_Enslaved enslaved_def = (Hediff_Enslaved)__instance.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Enslaved"));
				// Is the beating coming from the faction owning the slave?
				if(dinfo.Instigator != null)
				if (dinfo.Instigator.Faction == enslaved_def.slaverFaction) {
					enslaved_def.TakeWillpowerHit (dinfo.Amount);
				}
			}
		}
	}

	// Causes a slave to take a large willpower hit when caught after trying to escape
	[HarmonyPatch(typeof(JobDriver_TakeToBed), "CheckMakeTakeePrisoner")]
	public static class JobDriver_TakeToBed_Patch {

		static readonly PropertyInfo prisonerProp = AccessTools.Property(typeof(JobDriver_TakeToBed), "Takee");
		
		[HarmonyPostfix]
		public static void TakeToBed_Postfix(JobDriver_TakeToBed __instance){
			Pawn prisoner = (Pawn)prisonerProp.GetValue (__instance, null);
			if (prisoner.health.hediffSet.HasHediff (HediffDef.Named ("Enslaved"))) {
				// Caught the slave
				((Hediff_Enslaved)prisoner.health.hediffSet.GetFirstHediffOfDef (HediffDef.Named ("Enslaved"))).CaughtSlave ();
			}
		}
	}

//	// Keep track of when a slave collar is added
//	[HarmonyPatch(typeof(Pawn_ApparelTracker), "Notify_ApparelAdded")]
//	public static class ApparelAdded_Patch {
//		[HarmonyPostfix]
//		public static void ApparelAdded_Postfix(Pawn_ApparelTracker __instance, ref Apparel apparel){
//			// Slave collar
//			if (SlaveUtility.IsSlaveCollar(apparel)) {
//			//if (apparel.Stuff.tradeTags.Contains("SlaveCollar")){
//				if (!SlaveUtility.IsPawnColonySlave(__instance.pawn) && __instance.pawn.guest.IsPrisoner) {
//					__instance.pawn.health.AddHediff (HediffDef.Named ("Enslaved"));
//				}
//			}
//		}
//	}
	[HarmonyPatch(typeof(Pawn_ApparelTracker), "Notify_ApparelRemoved")]
	public static class ApparelRemoved_Patch {
		[HarmonyPostfix]
		public static void ApparelRemoved_Postfix(Pawn_ApparelTracker __instance, ref Apparel apparel){
			// Slave collar
			if (SlaveUtility.IsSlaveCollar(apparel)) {
				if (SlaveUtility.IsPawnColonySlave(__instance.pawn))
					SlaveUtility.EmancipatePawn (__instance.pawn);
			}
		}
	}

	// Stops slaves from trying to optimize apparel
	[HarmonyPatch(typeof(JobGiver_OptimizeApparel), "TryGiveJob")]
	public static class OptimizeApparel_Patch{
		[HarmonyPostfix]
		public static void TryGiveJob_Patch(ref Pawn pawn, ref Job __result){
			if (__result == null) return;
			if (__result.targetA.Thing != null && SlaveUtility.IsSlaveCollar (__result.targetA.Thing as Apparel)) {
				__result = null;
			}
			if (SlaveUtility.IsPawnColonySlave(pawn))
				__result = null;
		}
	}

	[HarmonyPatch(typeof(ITab_Pawn_Prisoner))]
	[HarmonyPatch("IsVisible", PropertyMethod.Getter)]
	public static class PrisonerTab_IsVisible_Patch{
		[HarmonyPostfix]
		public static void IsVisible_Patch(ref ITab_Pawn_Prisoner __instance, ref bool __result){
			var pawn = Find.Selector.SingleSelectedThing;
			if (pawn == null || pawn.GetType () != typeof(Pawn))
				return;

			if (__result == true) {
				if (SlaveUtility.IsPawnColonySlave(pawn as Pawn))
					__result = false;
			}
		}
	}

	// Add the custom label to the PIM Utility
	[HarmonyPatch(typeof(PrisonerInteractionModeUtility), "GetLabel")]
	public static class PIM_Utility_GetLabel_Patch{
		[HarmonyPostfix]
		public static void GetLabel_Patch(ref String __result, ref PrisonerInteractionModeDef mode){
			if (mode.defName == "PIM_Enslave")
				__result = "Enslave";
		}
	}

	[HarmonyPatch(typeof(PawnRenderer), "RenderPortait")]
	public static class PR_RP_Patch{
		[HarmonyPostfix]
		public static void RenderPortrait_Patch(ref PawnRenderer __instance){
			Pawn pawn = __instance.graphics.pawn;
			if (!SlaveUtility.IsPawnColonySlave(pawn))
				return;
			Apparel collar = pawn.apparel.WornApparel.Find (SlaveUtility.IsSlaveCollar);
			var chainColour = new Color(0.5f,0.5f,0.5f,0.5f);
			if (collar != null)
				chainColour = collar.DrawColor;

			Mesh mesh = MeshPool.humanlikeHeadSet.MeshAt (Rot4.South);
			Material mat = GraphicDatabase.Get<Graphic_Single> ("Things/Pawn/Humanlike/Apparel/SlaveCollar/Chain", ShaderDatabase.CutoutComplex, new Vector2 (1, 1), chainColour).MatAt (Rot4.South);
			mat.mainTexture.wrapMode = TextureWrapMode.Clamp;
			mat.mainTextureScale = new Vector2 (1.75f, 1.75f);
			mat.mainTextureOffset = new Vector2 (-0.37f, -0.10f);
			GenDraw.DrawMeshNowOrLater (mesh, new Vector3 (0,0.85f,0), Quaternion.identity, mat, true);
		}
	}

	[HarmonyPatch(typeof(GenLabel), "BestKindLabel")]
	public static class GenLabel_BKL_Patch{
		[HarmonyPostfix]
		public static void BestKindLabel_Patch(ref string __result, ref Pawn pawn, ref bool mustNoteGender){
			if(mustNoteGender && pawn.gender != Gender.None && SlaveUtility.IsPawnColonySlave(pawn)){
				__result = "PawnMainDescGendered".Translate ( new object[] { pawn.gender.GetLabel(), "LabelWordSlave".Translate()});
			}
		}
	}

	// Adds owned slaves to sellable 
	[HarmonyPatch(typeof(TradeUtility), "AllSellableColonyPawns")]
	public static class TradeUtility_ASCP_Patch{
		static IEnumerable<Pawn> sellableSlaves (Map map){
			foreach (Pawn pawn in map.mapPawns.FreeColonistsSpawned) {
				if (SlaveUtility.IsPawnColonySlave(pawn))
					yield return pawn;
			}
		}

		[HarmonyPostfix]
		public static void ASCP_Patch(ref IEnumerable<Pawn> __result, ref Map map){
			if(!__result.Contains(sellableSlaves(map).RandomElement()))
				__result = __result.Concat (sellableSlaves(map));
		}
	}

	// Adds a slave collar to generated slaves
	[HarmonyPatch(typeof(PawnGenerator), "GenerateGearFor")]
	public static class PawnGenerator_GP_Patch{
		[HarmonyPostfix]
		public static void GeneratePawn_Patch(ref PawnGenerationRequest request, ref Pawn pawn){
			if(request.KindDef == PawnKindDefOf.Slave && pawn.apparel.WornApparel.Find(SlaveUtility.IsSlaveCollar) != null){
				SlaveUtility.GiveSlaveCollar (pawn);
			}
		}
	}

	[HarmonyPatch(typeof(Pawn), "PreTraded")]
	public static class Pawn_PreTraded_Patch{
		[HarmonyPostfix]
		public static void PreTraded_Patch(ref Pawn __instance, ref TradeAction action){
			// Slave wearing a slave collar
			if (action == TradeAction.PlayerBuys && __instance.RaceProps.Humanlike && __instance.apparel.WornApparel.Find (SlaveUtility.IsSlaveCollar) != null) {
				// Add the enslaved tracker
				__instance.health.AddHediff (HediffDef.Named ("Enslaved"));
				// Set willpower to zero
				SlaveUtility.GetEnslavedHediff(__instance).SetWillpowerDirect (0);
				// Re-force wearing of the collar so the new slave does not drop it, freeing themselves
				__instance.outfits.forcedHandler.SetForced (__instance.apparel.WornApparel.Find (SlaveUtility.IsSlaveCollar), true);
			}
		}
	}

	[HarmonyPatch(typeof(Pawn), "CheckAcceptArrest")]
	public static class Pawn_CheckAcceptArrest_Patch{
		[HarmonyTranspiler]
		static IEnumerable<CodeInstruction> CheckArrest_Transpiler(IEnumerable<CodeInstruction> instructions)
		{
			foreach (CodeInstruction instruction in instructions)
			{
				MethodInfo slaveChance = typeof(SS_ArrestChance).GetMethod ("ArrestChance", AccessTools.all);
				//Log.Message (instruction.operand.ToStringSafe());
				if (instruction.operand.ToStringSafe() == "0.5") {
					yield return new CodeInstruction (OpCodes.Ldarg_0);
					yield return new CodeInstruction (OpCodes.Call, slaveChance);
				}
				else
					yield return instruction;
			}
		}
	}
	public static class SS_ArrestChance{
		public static float ArrestChance(Pawn pawn){
			//Log.Message ("Got here");
			return SlaveUtility.IsPawnColonySlave (pawn) ? 0.985f : 0.5f;
		}
	}
	
}

